Before you run test locally, make sure that one of conditions are met:
- you are in the office
- you are connected to office proxy (requires VPN)
- your public IP is present in `EXTRA_CIDR_BLOCKS` variable

Without that, `terraform apply` will fail on validating EKS cluster!

# Run examples locally

`terraform init`

`terraform apply -target random_integer.random_number`

`terraform apply`, `terraform plan` etc.

(It is important that you apply random number first and after that the rest of setup.)
