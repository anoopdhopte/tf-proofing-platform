variable "AWS_REGION" {
  description = "Provide aws region"
  type        = string
}

variable "PROJECT" {
  description = "Provide name of the project"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
  default     = []
}

variable "APPLICATION_CIDR_BLOCKS" {
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
  default     = []
}

variable "OFFICE_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of lodz office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "CI_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of CI"
  default     = ["18.192.228.102/32", "18.158.189.113/32", "18.192.153.221/32", "18.193.144.231/32"]
  type        = list(string)
}

variable "EXTRA_CIDR_BLOCKS" {
  description = "Provide extra CIDR blocks"
  default     = []
  type        = list(string)
}

variable "PROMETHEUS_EXTERNAL_LABEL" {
  description = "Prometheus label used to thanos connection"
  type        = string
}

variable "ADMIN_ROLES_ARN" {
  description = "Roles which should have access to kubernetes API"
  type        = list(string)
}

variable "KUBERNETES_VERSION" {
  description = "Version of kubernetes which should be deployed"
  type        = string
}

variable "AWS_LOGGING_ENABLED" {
  description = "Enable logging for all AWS components"
  type        = bool
}

variable "OBSERVABILITY_LOGGING_ROLE" {
  description = "Logging observabitlity IAM role arn"
  default     = ""
  type        = string
}

variable "KONG_EXTERNAL_DNS_ADDRESSES" {
  description = "List of dns addresses which should be accessible from kong ingress"
  type        = list(string)
}

variable "LOG_STREAM_NAME" {
  description = "Cloudwatch log stream name"
  type        = string
}

variable "ES_DOMAIN_ARN" {
  description = "ES domain arn"
  type        = string
}

variable "COGNITO_API_KEY" {
  description = "App api key for integration with corporate cognito"
  type        = string
}

variable "COGNITO_API_SECRET" {
  description = "App api key secret for integration with corporate cognito"
  type        = string
}

variable "FEDERATED_COGNITO_DOMAIN" {
  description = "Federated cognito public url"
  type        = string
}

variable "USER_POOL_ID" {
  description = "Cognito user pool ID"
  type        = string
}

variable "ISSUER_URL" {
  description = "Cognito issuer URL"
  type        = string
}

variable "ELASTICSEARCH_LOGGING_ENDPOINT" {
  description = "ES endpoint where logs will be pushed"
  default     = ""
  type        = string
}

variable "ELASTICSEARCH_LOGGING_REGION" {
  description = "ES region"
  default     = ""
  type        = string
}

variable "FLUENTD_REPLICAS" {
  description = "Number of replicas of fluentd"
  default     = 5
  type        = number
}

variable "APP_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "APP_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "APP_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 10
  type        = number
}

variable "MANAGEMENT_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 5
  type        = number
}
