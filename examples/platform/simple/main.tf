terraform {
  required_version = "~> 0.14.2"
}

provider "aws" {
  region = var.AWS_REGION
}

resource "random_integer" "random_number" {
  min = 1
  max = 9999
}

locals {
  environment   = random_integer.random_number.result
  dns_zone_name = "proofing-${local.environment}.cat-test.idemia.io"
}

module "proofing-platform" {
  source                         = "../../../modules/platform"
  AWS_REGION                     = var.AWS_REGION
  PROJECT                        = var.PROJECT
  ENVIRONMENT                    = local.environment
  ACCOUNT_NAME                   = var.ACCOUNT_NAME
  VPC_CIDR_BLOCK                 = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS             = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS        = var.APPLICATION_CIDR_BLOCKS
  DNS_ZONE_NAME                  = local.dns_zone_name
  PROMETHEUS_EXTERNAL_LABEL      = var.PROMETHEUS_EXTERNAL_LABEL
  ADMIN_ROLES_ARN                = var.ADMIN_ROLES_ARN
  KUBERNETES_VERSION             = var.KUBERNETES_VERSION
  AWS_LOGGING_ENABLED            = var.AWS_LOGGING_ENABLED
  KONG_EXTERNAL_DNS_ADDRESSES    = var.KONG_EXTERNAL_DNS_ADDRESSES
  LOG_STREAM_NAME                = var.LOG_STREAM_NAME
  ES_DOMAIN_ARN                  = var.ES_DOMAIN_ARN
  FEDERATED_COGNITO_DOMAIN       = var.FEDERATED_COGNITO_DOMAIN
  USER_POOL_ID                   = var.USER_POOL_ID
  ISSUER_URL                     = var.ISSUER_URL
  COGNITO_API_KEY                = var.COGNITO_API_KEY
  COGNITO_API_SECRET             = var.COGNITO_API_SECRET
  ELASTICSEARCH_LOGGING_ENDPOINT = var.ELASTICSEARCH_LOGGING_ENDPOINT
  ELASTICSEARCH_LOGGING_REGION   = var.ELASTICSEARCH_LOGGING_REGION
  FLUENTD_REPLICAS               = var.FLUENTD_REPLICAS
  OBSERVABILITY_LOGGING_ROLE     = var.OBSERVABILITY_LOGGING_ROLE
  APP_NR_OF_NODES                = var.APP_NR_OF_NODES
  APP_MIN_NR_OF_NODES            = var.APP_MIN_NR_OF_NODES
  APP_MAX_NR_OF_NODES            = var.APP_MAX_NR_OF_NODES
  MANAGEMENT_NR_OF_NODES         = var.MANAGEMENT_NR_OF_NODES
  MANAGEMENT_MIN_NR_OF_NODES     = var.MANAGEMENT_MIN_NR_OF_NODES
  MANAGEMENT_MAX_NR_OF_NODES     = var.MANAGEMENT_MAX_NR_OF_NODES
}

data "aws_route53_zone" "main" {
  name         = "cat-test.idemia.io"
  private_zone = false
}

module "delegation" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/delegation?ref=1.1.0"
  NAME_SERVERS  = module.proofing-platform.dns_public_name_servers
  DNS_ZONE_NAME = module.proofing-platform.dns_zone_name
  ZONE_ID       = data.aws_route53_zone.main.id
}
