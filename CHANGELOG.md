# Change Log
-------------
## 1.2.0 TBD
Features:
- Adopt EKS addons (vpc-cni, coredns) and customize vpc-cni settings [SIE-3413](https://idemiadigitallabs.atlassian.net/browse/SIE-3413)
- Replace Kong ingress with Istio [SIE-4323](https://idemiadigitallabs.atlassian.net/browse/SIE-4323)
  - [Required changes for applications relying on Kong](https://idemiadigitallabs.atlassian.net/wiki/spaces/SIE/pages/3175909045/Prepare+application+to+platform+release+2.4.15+app+and+1.2.0+WR)
- Simplify Route 53 configuration by replacing app-specific Kong/Istio aliases with single wildcard record
- Upgrade Vault Helm chart and images to latest versions to fix token issue after pod restart
- Apps deployed by terraform now do not have CPU limits defined and their memory limits are equal to memory requests
- Removed ephemral storage declaration for prometheus-server inside istio

New variables:
- (optional) `CLUSTER_ADDONS` - Map of cluster addon configurations to enable for the cluster. Addon name can be the map keys or set with `name`
- (optional) `WARM_ENI_TARGET` - Specifies the number of ENIs the daemon should keep ready
- (optional) `WARM_IP_TARGET` - Specifies the number of IPs that will be reserved for the node-ENI pair
- (optional) `MINIMUM_IP_TARGET` - Specifies how many minimum number of IPs to keep around each node
- (optional) `ISTIO_INGRESS_ALB_BACKEND_PORT` - Node port where Istio ingress gateway will listen to requests
- (optional) `ISTIO_INGRESS_ALB_BACKEND_PROTOCOL` - Istio Ingress load balancer backend protocol, ex TLS, HTTP
- (optional) `ISTIO_INGRESS_ALB_HEALTH_CHECK_PORT` - Istio Ingress load balancer health check port
- (optional) `DEFAULT_RESOURCES_MUTATING_WEBHOOK_MEMORY`
- (optional) `DEFAULT_MEMORY`
- (optional) `FLUENTD_MEMORY`
- (optional) `FLUENTD_CONFIG_RELOADER_MEMORY`
- (optional) `CLUSTER_AUTOSCALER_MEMORY`
- (optional) `EXTERNAL_DNS_MEMORY`
- (optional) `KUBE2IAM_MEMORY`
- (optional) `ISTIO_OPERATOR_MEMORY`
- (optional) `LOGGING_OPERATOR_MEMORY`
- (optional) `JAEGER_SCHEMA_MEMORY`
- (optional) `JAEGER_AGENT_MEMORY`
- (optional) `JAEGER_QUERY_MEMORY`
- (optional) `JAEGER_CASSANDRA_MEMORY`
- (optional) `KIALI_OPERATOR_MEMORY`
- (optional) `KIALI_MEMORY`
- (optional) `NGINX_INGRESS_MEMORY`
- (optional) `KUBED_MEMORY`
- (optional) `VAULT_INJECTOR_MEMORY`
- (optional) `VAULT_SERVER_MEMORY`
- (optional) `ISTIO_PROMETHEUS_SERVER_MEMORY` 
- (optional) `ISTIO_PROMETHEUS_CONFIG_RELOADER_MEMORY`

Removed variables:
- All variables containing `KONG`
- `CUSTOM_INGRESS_ALB_BACKEND_PORT`
- `CUSTOM_INGRESS_ALB_BACKEND_PROTOCOL`
- `CUSTOM_INGRESS_ALB_HEALTH_CHECK_PORT`
- `CUSTOM_INGRESS_ALB_HEALTH_CHECK_URL`
- `INSTALL_KONG_INGRESS`
- `EXTRA_INTERNAL_TO_PUBLIC_DNS_ALIASES`
- All variables that defined memory requests/limits for argo-apps module
- `DEFAULT_RESOURCES_MUTATING_WEBHOOK_MEMORY_REQUEST`
- `DEFAULT_RESOURCES_MUTATING_WEBHOOK_MEMORY_LIMIT`
- `DEFAULT_MEMORY_LIMIT`
- `DEFAULT_MEMORY_REQUEST`
- `FLUENTD_MEMORY_LIMIT`
- `FLUENTD_CONFIG_RELOADER_MEMORY_REQUEST`
- `FLUENTD_CONFIG_RELOADER_MEMORY_LIMIT`
- `CLUSTER_AUTOSCALER_MEMORY_REQUEST`
- `CLUSTER_AUTOSCALER_MEMORY_LIMIT`
- `EXTERNAL_DNS_MEMORY_REQUEST`
- `EXTERNAL_DNS_MEMORY_LIMIT`
- `KUBE2IAM_MEMORY_REQUEST`
- `KUBE2IAM_MEMORY_LIMIT`
- `ISTIO_OPERATOR_MEMORY_REQUEST`
- `ISTIO_OPERATOR_MEMORY_LIMIT`
- `LOGGING_OPERATOR_MEMORY_REQUEST`
- `LOGGING_OPERATOR_MEMORY_LIMIT`
- `JAEGER_SCHEMA_MEMORY_REQUEST`
- `JAEGER_SCHEMA_MEMORY_LIMIT`
- `JAEGER_AGENT_MEMORY_LIMIT`
- `JAEGER_AGENT_MEMORY_REQUEST`
- `JAEGER_COLLECTOR_MEMORY_REQUEST`
- `JAEGER_COLLECTOR_MEMORY_LIMIT`
- `JAEGER_QUERY_MEMORY_LIMIT`
- `JAEGER_QUERY_MEMORY_REQUEST`
- `JAEGER_CASSANDRA_MEMORY_REQUEST`
- `JAEGER_CASSANDRA_MEMORY_LIMIT`
- `KIALI_OPERATOR_MEMORY_LIMIT`
- `KIALI_OPERATOR_MEMORY_REQUEST`
- `KIALI_MEMORY_LIMIT`
- `KIALI_MEMORY_REQUEST`
- `NGINX_INGRESS_MEMORY_LIMIT`
- `NGINX_INGRESS_MEMORY_REQUEST`
- `KUBED_MEMORY_REQUEST`
- `KUBED_MEMORY_LIMIT`
- `VAULT_INJECTOR_MEMORY_LIMIT`
- `VAULT_INJECTOR_MEMORY_REQUEST`
- `VAULT_SERVER_MEMORY_LIMIT`
- `VAULT_SERVER_MEMORY_REQUEST`
- `ISTIO_PROMETHEUS_SERVER_MEMORY_LIMIT`
- `ISTIO_PROMETHEUS_SERVER_MEMORY_REQUEST`
- `ISTIO_PROMETHEUS_CONFIG_RELOADER_MEMORY_LIMIT`
- `ISTIO_PROMETHEUS_CONFIG_RELOADER_MEMORY_REQUEST`

New outputs:
- `cluster_oidc_issuer` - OIDC issuer associated with cluster

## 1.1.9
Features:
- Add default resources for pods [[Documentation](https://idemiadigitallabs.atlassian.net/wiki/spaces/GID/pages/2876735508/OnePlatform+Default+pods+resources+mechanism)]
- Add K8s resources and node selectors to deployments without them
- Vault-injector will no longer be forever outOfSync in ArgoCD. Its mutating webhook will be ignored by ArgoCD diffing tool.

New variables:
- INSTALL_DEFAULT_RESOURCES_MUTATING_WEBHOOK flag which enables mutating webhook implementing default pods' resources
- Configuration of default resources (applies only if INSTALL_DEFAULT_RESOURCES_MUTATING_WEBHOOK flag is enabled)
    - DEFAULT_CPU_REQUEST (250m by default)
    - DEFAULT_MEMORY_REQUEST (100Mi by default)
    - DEFAULT_CPU_LIMIT (256m by default)
    - DEFAULT_MEMORY_LIMIT (256Mi by default)
- Variables for ArgoCD
    - IGNORE_MUTATING_WEBHOOKS - Boolean value for making ArgoCD ignore mutating webhooks when looking for diffs between live and desired manifests. (Default: true)

Deleted variables:
- HERMES_MANAGEMENT_INGRESS
- INSTALL_HERMES
- INSTALL_SCHEMA_REGISTRY_UI

## 1.1.8 2021-11-18
Features:
- Refactor CIDR blocks

## 1.1.7 2021-11-09
Features:
- Update nessus scanner version

## 1.1.6 2021-10-05
Features:
- New argo-apps-deployment version - 1.2.20
- egress rules to db cluster
Fixes:
- fix logging problems with wrong timestamp parsing
New variables:
- EKS_APP_TARGET_GROUP_ARNS which gives possibility to configure additional ingress ARN to add to target APP group
- EKS_MANAGEMENT_TARGET_GROUP_ARNS which gives possibility to configure additional ingress ARN to add to target MANAGEMENT group
- 4 new variables to configure fluentd buffer for elasticsearch output

## 1.1.5 2021-09-22
Features:
- Upgrade of argo-apps-deployment module to 1.2.16
- Upgrade of Kiali
- Added support for DNS peering
- Added VPC flows logging
- Added parameter for alertmanager
- new common module
Fixes:
- s3 secure connection
- allow redis on egress rules
New variables:
- 4 new variables to configure limits and requests of fluentd pods
- ENABLED_JAEGER_INGRESS which gives possibility to enable ingress for Jaeger 
- ADDITIONAL_CLIENTS which gives possibility to add app clients to Cognito user pool
- EXTRA_INTERNAL_DNS_ALIASES  which gives possibility to add extra aliases which should be added to private dns zone which will point to nginx ingress
- SINGLE_NAT_GATEWAY you can define whether only single nat gateway should be created (for prod envs should be false)

## 1.1.4 2021-08-24
Features:
- Nessus scanner as optional element of platform
New variables:
- ADDITIONAL_SSH_KEYS - Addtional ssh keys which can be added to kubernetes nodes
- ENABLE_NESSUS - Boolean value which starts or stops nessus scanner
- AWS_ASSUME_ROLE which gives possibility to use another Terraform role
- ENABLED_JAEGER_INGRESS gives possibility to hide Jaeger from internet
## 1.1.3 2021-08-10
Features:
- updated logging helm charts
- option to set number of fluentd replicas
- new nginx variables for external DNS 
- new outputs

## 1.1.2 2021-07-23
Features:
- spot instances
- jaeger ingress variable

Bug Fixes:
- kong healthcheck

## 1.1.1 2021-07-09
Fixes:
- Fix issue with missing kong-kubernetes-ingress-controller

## 1.0.1 2021-02-16
Features added:
   - Cognito
   - kiali cognito authentication

## 1.0.1 2021-02-16
Features added:
   - Kiali operator
   - Jaeger - prod mode
   - Hermes improvements
   - msk - configuration

Bug fixes:
   - Logging performance improvement
   - ingress paths cofiguration


## 1.0.0 2021-02-08
Features:
   - vpc
   - eks cluster api - 1.18
   - autoscaling group for eks nodes 1.18
   - alb for tools
   - kong nlb for app
   - apps on cluster:
       - argocd
       - kong
       - prometheus-operator
       - fluent-bit
       - kube2iam
       - externaldns
       - cluster autoscaller
       - hermes
       - istio
       - kiali
       - metrics-server
       - nginx-ingress

## 0.1.0 2020-10-26
Initial package

#### Added
- Initial package for eks-app-platform
