provider "aws" {
  region = var.AWS_REGION
  assume_role {
    role_arn     = var.AWS_ASSUME_ROLE
    session_name = "app-platform"
  }
}

provider "aws" {
  region = "us-east-1"
  alias  = "certificates"
  assume_role {
    role_arn     = var.AWS_ASSUME_ROLE
    session_name = "app-platform"
  }
}

locals {
  role_arn_arg = var.AWS_ASSUME_ROLE != "" ? ["--role-arn", var.AWS_ASSUME_ROLE] : []
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = concat(["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name], local.role_arn_arg)
    command     = "aws"
  }
}

provider "helm" {
  kubernetes {
    host                   = data.aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
    exec {
      api_version = "client.authentication.k8s.io/v1alpha1"
      args        = concat(["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.name], local.role_arn_arg)
      command     = "aws"
    }
  }
}
