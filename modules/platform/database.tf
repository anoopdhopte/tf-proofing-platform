module "db" {
  count               = var.ENABLE_DATABASES == true ? 1 : 0
  source              = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/aurora?ref=1.0.4"
  globals             = module.common.globals
  networking          = module.networking.common
  DB_NAME             = var.DB_NAME
  ADMIN_USER          = var.DB_ADMIN_USER
  ADMIN_PASSWORD      = var.DB_ADMIN_PASSWORD
  SUBNETS             = module.networking.application_subnet_ids
  ALLOWED_CIDR_BLOCKS = var.DB_ALLOWED_CIDR_BLOCKS
  AUTOSCALING_ENABLED = var.DB_AUTOSCALING_ENABLED
  ENGINE_MODE         = var.DB_ENGINE_MODE
  PUBLICLY_ACCESSIBLE = false
  ENGINE              = var.DB_ENGINE
  ENGINE_VERSION      = var.DB_ENGINE_VERSION
  INSTANCE_TYPE       = var.DB_INSTANCE_TYPE
  SCALING_CONFIGURATION = [
    {
      auto_pause               = false
      max_capacity             = 4
      min_capacity             = 2
      seconds_until_auto_pause = 300
    }
  ]
}

module "redis" {
  count                  = var.REDIS_ENABLED == true ? 1 : 0
  source                 = "git::https://bitbucket.org/anoopdhopte/elasticache.git//modules/redis?ref=1.0.4"
  globals                = module.common.globals
  networking             = module.networking.common
  KMS_KEY_ID             = module.encryption.kms_key_id
  SUBNET_IDS             = module.networking.application_subnet_ids
  INGRESS_CIDR_BLOCKS    = var.DB_ALLOWED_CIDR_BLOCKS
  REPLICATION_GROUP_ID   = var.REDIS_GROUP_ID
  AUTH_TOKEN             = var.AUTH_TOKEN
  ENGINE_VERSION         = var.REDIS_ENGINE_VERSION
  CUSTOM_PARAMETER_GROUP = var.REDIS_CUSTOM_PARAMETER_GROUP
  APPLY_IMMEDIATELY      = var.REDIS_APPLY_IMMEDIATELY
  NODE_GROUPS            = 3
}