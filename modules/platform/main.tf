resource "random_string" "id" {
  length  = 4
  special = false
  upper   = true
  lower   = false
  number  = false
}

module "common" {
  source                = "git::https://bitbucket.org/anoopdhopte/common.git//modules?ref=1.2.1"
  ACCOUNT_NAME          = var.ACCOUNT_NAME
  AWS_REGION            = var.AWS_REGION
  ENVIRONMENT           = var.ENVIRONMENT
  PROJECT               = var.PROJECT
  KMS_SHARED_AMI_KEY_ID = var.KMS_SHARED_AMI_KEY_ID
  AMI_OWNER             = var.AMI_OWNER
}

module "encryption" {
  source              = "git::https://bitbucket.org/anoopdhopte/encryption.git//modules?ref=1.0.3"
  globals             = module.common.globals
  KMS_KEY_NAME_SUFFIX = random_string.id.result
}

module "dns_zone" {
  source                  = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/zone_public?ref=1.0.2"
  globals                 = module.common.globals
  DNS_ZONE_NAME           = var.DNS_ZONE_NAME
  CREATE_ZONE_CERTIFICATE = var.AWS_CERT == true ? true : false
}

module "private_dns_zone" {
  count           = local.create_private_zone ? 1 : 0
  source          = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/zone_private?ref=1.1.0"
  globals         = module.common.globals
  networking      = module.networking.common
  DNS_ZONE_NAME   = var.DNS_ZONE_NAME
  ADDITIONAL_VPCS = var.PRIVATE_ZONE_ADDITIONAL_VPCS
}

resource "aws_route53_zone_association" "vpc_zone_association" {
  count   = length(var.ADDITIONAL_ASSOCIATED_ZONES)
  vpc_id  = module.networking.vpc_id
  zone_id = var.ADDITIONAL_ASSOCIATED_ZONES[count.index]
}

module "networking" {
  source                  = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/networking?ref=2.0.3"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
  FLOW_LOGS_ENABLED       = var.FLOW_LOGS_ENABLED
  FLOW_LOGS_BUCKET_ARN    = module.logs_bucket.s3_bucket_arn
  FLOW_LOGS_TRAFFIC_TYPE  = var.FLOW_LOGS_TRAFFIC_TYPE  
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = var.SINGLE_NAT_GATEWAY
  CREATE_PRIVATE_DNS_ZONE = false
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.name
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.name
}

module "ingress_alb" {
  source                     = "git::https://bitbucket.org/anoopdhopte/ingress.git//modules/alb?ref=1.1.1"
  globals                    = module.common.globals
  networking                 = module.networking.common
  PUBLIC_SUBNET_IDS          = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN   = var.AWS_CERT == true ? module.dns_zone.wildcard_certificate_arn : ""
  INTERNAL                   = false
  INGRESS_ACCESS_CIDR        = concat(var.OFFICE_CIDR_BLOCKS,var.CI_CIDR_BLOCKS,var.EXTRA_CIDR_BLOCKS)
  ACCESS_LOGS_BUCKET_NAME    = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : ""
  ACCESS_LOGS_BUCKET_PREFIX  = var.AWS_LOGGING_ENABLED == true ? "management-ingress" : ""
  ENABLE_DELETION_PROTECTION = var.ENABLE_DELETION_PROTECTION
}

module "local_ingress_alb_eks" {
  source                     = "git::https://bitbucket.org/anoopdhopte/ingress.git//modules/alb?ref=1.1.1"
  globals                    = module.common.globals
  networking                 = module.networking.common
  PUBLIC_SUBNET_IDS          = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN   = var.AWS_CERT == true ? module.dns_zone.wildcard_certificate_arn : ""
  INTERNAL                   = true
  ACCESS_LOGS_BUCKET_NAME    = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : ""
  ACCESS_LOGS_BUCKET_PREFIX  = var.AWS_LOGGING_ENABLED == true ? "internal-ingress" : ""
  ENABLE_DELETION_PROTECTION = var.ENABLE_DELETION_PROTECTION
}

module "istio_ingress_alb" {
  source                     = "git::https://bitbucket.org/anoopdhopte/ingress.git//modules/alb?ref=1.1.1"
  globals                    = module.common.globals
  networking                 = module.networking.common
  PUBLIC_SUBNET_IDS          = module.networking.public_subnet_ids
  BALANCER_CERTIFICATE_ARN   = var.AWS_CERT == true ? module.dns_zone.wildcard_certificate_arn : ""
  INTERNAL                   = false
  BACKEND_PORT               = var.ISTIO_INGRESS_ALB_BACKEND_PORT
  BACKEND_PROTOCOL           = var.ISTIO_INGRESS_ALB_BACKEND_PROTOCOL
  HEALTH_CHECK_PORT          = var.ISTIO_INGRESS_ALB_HEALTH_CHECK_PORT
  HEALTH_CHECK_URL           = "/healthz/ready"
  HEALTH_CHECK_MATCHER       = "200"
  INGRESS_ACCESS_CIDR        = var.APP_INGRESS_ACCESS_CIDR
  ACCESS_LOGS_BUCKET_NAME    = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : ""
  ACCESS_LOGS_BUCKET_PREFIX  = var.AWS_LOGGING_ENABLED == true ? "application-ingress" : ""
  ENABLE_DELETION_PROTECTION = var.ENABLE_DELETION_PROTECTION
}

module "istio_wildcard_alias_public" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.2"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = module.istio_ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID = module.istio_ingress_alb.ingress_zone_id
  DOMAIN_NAMES  = ["*.${var.DNS_ZONE_NAME}"]
}

module "istio_wildcard_alias_private" {
  count         = local.create_private_zone ? 1 : 0
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.2"
  ZONE_ID       = module.private_dns_zone[0].zone_id
  ALIAS_NAME    = module.istio_ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID = module.istio_ingress_alb.ingress_zone_id
  DOMAIN_NAMES  = ["*.${var.DNS_ZONE_NAME}"]
}

module "nginx_addons_aliases" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.0.2"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = module.ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID = module.ingress_alb.ingress_zone_id
  DOMAIN_NAMES  = var.NGINX_EXTERNAL_DNS_ADDRESSES
}

module "local_nginx_addons_aliases" {
  count         = local.create_private_zone ? 1 : 0
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.0.2"
  ZONE_ID       = module.private_dns_zone[0].zone_id
  ALIAS_NAME    = module.local_ingress_alb_eks.ingress_dns_name
  ALIAS_ZONE_ID = module.local_ingress_alb_eks.ingress_zone_id
  DOMAIN_NAMES  = var.LOCAL_NGINX_EXTERNAL_DNS_ADDRESSES
}

module "waf" {
  count                 = var.ENABLE_COMMUNITY_WAF == true ? 1 : 0
  source                = "git::https://bitbucket.org/anoopdhopte/waf.git//modules/regional?ref=1.0.3"
  globals               = module.common.globals
  WAF_PREFIX            = "${var.PROJECT}-${var.ENVIRONMENT}"
  ALB_ARN               = [module.ingress_alb.ingress_arn]
  S3_ACCESS_LOGS_BUCKET = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : null
  MFA_DELETE            = var.MFA_ENABLED
}

module "waf_app" {
  count                 = var.ENABLE_COMMUNITY_WAF == true ? 1 : 0
  source                = "git::https://bitbucket.org/anoopdhopte/waf.git//modules/regional?ref=1.0.3"
  globals               = module.common.globals
  WAF_PREFIX            = "${var.PROJECT}-${var.ENVIRONMENT}"
  ALB_ARN               = [module.istio_ingress_alb.ingress_arn]
  S3_ACCESS_LOGS_BUCKET = var.AWS_LOGGING_ENABLED == true ? module.logs_bucket.s3_bucket_name : null
  MFA_DELETE            = var.MFA_ENABLED
}

module "addons_aliases" {
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.0.2"
  ZONE_ID       = module.dns_zone.zone_id
  ALIAS_NAME    = module.ingress_alb.ingress_dns_name
  ALIAS_ZONE_ID = module.ingress_alb.ingress_zone_id
  DOMAIN_NAMES  = concat(["management-apps.${var.DNS_ZONE_NAME}"], var.EXTRA_PUBLIC_DNS_ALIASES)
}

module "internal_addons_aliases" {
  count         = local.create_private_zone ? 1 : 0
  source        = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/alias?ref=1.1.0"
  ZONE_ID       = module.private_dns_zone[0].zone_id
  ALIAS_NAME    = module.local_ingress_alb_eks.ingress_dns_name
  ALIAS_ZONE_ID = module.local_ingress_alb_eks.ingress_zone_id
  DOMAIN_NAMES  = var.EXTRA_INTERNAL_DNS_ALIASES
}
