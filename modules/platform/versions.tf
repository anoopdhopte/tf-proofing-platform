terraform {
  required_version = ">= 0.14.0"

  required_providers {
    # This is the minimal version of the provider(s) required by this module to work properly
    # Update it only if changes in this module really required features provided by this version
    # Don't update blindly as we will use latest anyway.
    aws        = "<= 3.37"
    null       = ">= 2.1.2"
    local      = ">= 1.4"
    kubernetes = ">= 1.11.1"
  }
}
