resource "aws_security_group" "proofing_node" {
  name        = "${local.environment_name}-waitingroom-sg"
  description = "Security group for all additional external and internal rules"
  vpc_id      = module.networking.vpc_id

  tags = merge(
    map(
      "CreatedBy", "terraform",
      "Name", "${local.environment_name}-waitingroom-security-group",
      "Object", "aws_security_group"
    )
  )
}