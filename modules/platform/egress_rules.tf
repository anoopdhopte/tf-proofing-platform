resource "aws_security_group_rule" "database_egress_5432" {
  description       = "Allow outgoing network flow on port 5432/tcp - aurora-pg"
  type              = "egress"
  from_port         = 5432
  to_port           = 5432
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.proofing_node.id
}

resource "aws_security_group_rule" "database_egress_6379" {
  description       = "Allow outgoing network flow on port 6379/tcp - redis"
  type              = "egress"
  from_port         = 6379
  to_port           = 6379
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.proofing_node.id
}

resource "aws_security_group_rule" "database_egress_9042" {
  description       = "Allow outgoing network flow on port 9042/tcp - cassandra"
  type              = "egress"
  from_port         = 9042
  to_port           = 9042
  protocol          = "tcp"
  cidr_blocks       = var.DATABASE_CIDR_BLOCK
  security_group_id = aws_security_group.proofing_node.id
}