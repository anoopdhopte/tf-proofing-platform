module "eks" {
  source                    = "git::https://bitbucket.org/anoopdhopte/eks.git//modules/cluster?ref=2.0.14"
  globals                   = module.common.globals
  networking                = module.networking.common
  SUBNET_IDS                = module.networking.application_subnet_ids
  KMS_KEY_ID                = module.encryption.kms_key_id
  PUBLIC_ACCESS_CIDRS       = concat(var.OFFICE_CIDR_BLOCKS,var.CI_CIDR_BLOCKS,var.EXTRA_CIDR_BLOCKS)
  KUBERNETES_VERSION        = var.KUBERNETES_VERSION
  ADMIN_ROLES_ARN           = var.ADMIN_ROLES_ARN
  CREATE_KUBECONFIG         = true
  ENABLED_CLUSTER_LOG_TYPES = var.AWS_LOGGING_ENABLED == true ? ["api", "audit", "authenticator", "controllerManager", "scheduler"] : null
}

module "eks_nodes_management_apps" {
  source             = "git::https://bitbucket.org/anoopdhopte/eks.git//modules/node?ref=2.0.10"
  cluster            = module.eks.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  KUBERNETES_VERSION = var.KUBERNETES_VERSION
  INSTANCE_TYPE      = var.MANAGEMENT_INSTANCE_TYPE
  SUBNET_IDS         = module.networking.application_subnet_ids
  CUSTOM_TAGS        = local.autoshutdown_tag
  ASG_CUSTOM_TAGS    = [local.autoshutdown_asg_tag]
  SECURITY_GROUPS_ID = [
    module.networking.default_security_group_id,
    module.ingress_alb.ingress_to_nodes_security_group,
    module.local_ingress_alb_eks.ingress_to_nodes_security_group,
    aws_security_group.proofing_node.id
  ]
  DOCKER_REGISTRY_CREDENTIALS = var.DOCKER_REGISTRY_CREDENTIALS
  TARGET_GROUP_ARNS           = concat(var.EKS_MANAGEMENT_TARGET_GROUP_ARNS, [module.ingress_alb.ingress_80_arn, module.local_ingress_alb_eks.ingress_80_arn])
  MAX_NR_OF_NODES             = var.MANAGEMENT_MAX_NR_OF_NODES
  MIN_NR_OF_NODES             = var.MANAGEMENT_MIN_NR_OF_NODES
  NR_OF_NODES                 = var.MANAGEMENT_NR_OF_NODES
  ENABLE_SPOT_INSTANCES       = var.MANAGEMENT_ENABLE_SPOT_INSTANCES
  SPOT_MAX_PRICE              = var.MANAGEMENT_SPOT_MAX_PRICE
  VOLUME_SIZE                 = var.MANAGEMENT_NODES_VOLUMES_SIZE
  VOLUME_TYPE                 = var.MANAGEMENT_NODES_VOLUMES_TYPE
  SUFFIX                      = "-management"
  KUBELET_EXTRA_ARGS          = "--node-labels=zone=management-apps"
  ADDITIONAL_SSH_KEYS         = var.ADDITIONAL_SSH_KEYS
  depends_on                  = [module.eks]
}

module "eks_nodes_app" {
  source             = "git::https://bitbucket.org/anoopdhopte/eks.git//modules/node?ref=2.0.10"
  cluster            = module.eks.cluster
  globals            = module.common.globals
  KMS_KEY_ID         = module.encryption.kms_key_id
  KUBERNETES_VERSION = var.KUBERNETES_VERSION
  INSTANCE_TYPE      = var.APP_INSTANCE_TYPE
  SUBNET_IDS         = module.networking.application_subnet_ids
  CUSTOM_TAGS        = local.autoshutdown_tag
  ASG_CUSTOM_TAGS = concat([
    { key = "k8s.io/cluster-autoscaler/enabled", value = "true", propagate_at_launch = true },
    { key = "k8s.io/cluster-autoscaler/${module.eks.name}", value = "true", propagate_at_launch = true }],
  [local.autoshutdown_asg_tag])
  SECURITY_GROUPS_ID = [
    module.networking.default_security_group_id,
    module.istio_ingress_alb.ingress_to_nodes_security_group,
    aws_security_group.proofing_node.id
  ]
  DOCKER_REGISTRY_CREDENTIALS = var.DOCKER_REGISTRY_CREDENTIALS
  TARGET_GROUP_ARNS           = concat(var.EKS_APP_TARGET_GROUP_ARNS, [module.istio_ingress_alb.ingress_80_arn])
  MAX_NR_OF_NODES             = var.APP_MAX_NR_OF_NODES
  MIN_NR_OF_NODES             = var.APP_MIN_NR_OF_NODES
  NR_OF_NODES                 = var.APP_NR_OF_NODES
  ENABLE_SPOT_INSTANCES       = var.APP_ENABLE_SPOT_INSTANCES
  SPOT_MAX_PRICE              = var.APP_SPOT_MAX_PRICE
  VOLUME_SIZE                 = var.APP_NODES_VOLUMES_SIZE
  VOLUME_TYPE                 = var.APP_NODES_VOLUMES_TYPE
  SUFFIX                      = "-app"
  KUBELET_EXTRA_ARGS          = "--node-labels=zone=app"
  ADDITIONAL_SSH_KEYS         = var.ADDITIONAL_SSH_KEYS
  depends_on                  = [module.eks]
}

module "ssm" {
  source    = "git::https://bitbucket.org/anoopdhopte/iam.git//modules/role_policy_attachment?ref=1.0.1"
  ROLE_NAME = module.eks.node_role_name
  POLICIES  = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
}

module "eks_addons" {
  source         = "git::https://bitbucket.org/anoopdhopte/eks.git//modules/addons?ref=2.1.0"
  CLUSTER_NAME   = module.eks.name
  globals        = module.common.globals
  CLUSTER_ADDONS = var.CLUSTER_ADDONS

  depends_on = [
    module.eks,
    module.eks_nodes_management_apps,
    module.eks_nodes_app,
  ]
}

resource "null_resource" "vpc_cni_customization" {
  triggers = {
    creation_time = module.eks_addons.cluster_addons["vpc-cni"].created_at
  }

  provisioner "local-exec" {
    environment = {
      KUBECONFIG = module.eks.kubeconfig_filename
    }
    command = "kubectl set env ds aws-node -n kube-system WARM_ENI_TARGET=${var.WARM_ENI_TARGET} WARM_IP_TARGET=${var.WARM_IP_TARGET} MINIMUM_IP_TARGET=${var.MINIMUM_IP_TARGET}"
  }

  depends_on = [module.eks_addons]
}
