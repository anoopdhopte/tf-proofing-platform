output "common_globals" {
  description = "Return common globals"
  value       = module.common.globals
}

output "dns_public_name_servers" {
  description = "Return public name servers"
  value       = module.dns_zone.public_name_servers
}

output "dns_zone_id" {
  description = "Return public zone id"
  value       = module.dns_zone.zone_id
}

output "dns_zone_name" {
  description = "Return public zone name"
  value       = module.dns_zone.zone_name
}

output "dns_zone_wildcard_certifcate_arn" {
  description = "Return dns_zone wildcard certificate"
  value       = module.dns_zone.wildcard_certificate_arn
}

output "networking_common" {
  description = "Return networking globals"
  value       = module.networking.common
}

output "networking_public_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.public_route_table_ids
}

output "networking_vpc_id" {
  description = "Return networking vpc id"
  value       = module.networking.vpc_id
}

output "networking_public_subnets" {
  description = "Return public subnets"
  value       = module.networking.public_subnets
}

output "networking_public_subnet_ids" {
  description = "Return public subnets id"
  value       = module.networking.public_subnet_ids
}

output "networking_application_subnets" {
  description = "Return application subnets"
  value       = module.networking.application_subnets
}

output "networking_application_subnet_ids" {
  description = "Return application subnets id"
  value       = module.networking.application_subnet_ids
}

output "networking_application_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.application_route_table_ids
}

output "networking_data_route_table_ids" {
  description = "Return networking data_route_table_ids"
  value       = module.networking.data_route_table_ids
}

output "networking_application_availability_zones" {
  description = "Return networking application AZ"
  value       = module.networking.application_availability_zones
}

output "aws_security_group_proofing_node_id" {
  description = "Security group which can be extended for extra ingress/egress rules"
  value       = aws_security_group.proofing_node.id
}

output "encryption_kms_key_id" {
  description = "Return encryption kms_key_id"
  value       = module.encryption.kms_key_id
}

output "ingress_alb_dns_name" {
  description = "Return ingress alb dns name"
  value       = module.ingress_alb.ingress_dns_name
}

output "eks_cluster" {
  description = "Return common networking variables used by other modules"
  value       = "module.eks.cluster"
}

output "eks_name" {
  description = "Return name of eks cluster"
  value       = module.eks.name
}

output "eks_endpoint" {
  description = "Return endpoint of eks cluster"
  value       = module.eks.endpoint
}

output "eks_kubeconfig_filename" {
  description = "Return eks kubeconfig filename"
  value       = module.eks.kubeconfig_filename
}

output "eks_node_role_arn" {
  description = "Return eks nodes arn"
  value       = module.eks.node_role_arn
}

output "argocd_name" {
  description = "Return argocd release metadata name"
  value       = module.argocd.name
}

output "argocd_chart_name" {
  description = "Return argocd release metadata chart name"
  value       = module.argocd.chart_name
}

output "argocd_revision" {
  description = "Return argocd release metadata chart revision"
  value       = module.argocd.revision
}

output "argocd_namespace" {
  description = "Return argocd release metadata namespace"
  value       = module.argocd.namespace
}

output "argocd_version" {
  description = "Return argocd release metadata chart version"
  value       = module.argocd.version
}

output "argocd_app_version" {
  description = "Return argocd release metadata chart app_version"
  value       = module.argocd.app_version
}

output "cluster_oidc_issuer" {
  description = "OIDC issuer associated with cluster"
  value = module.eks.cluster_oidc_issuer
}
