locals {
  environment_name     = "${var.PROJECT}-${var.ENVIRONMENT}"
  autoshutdown_tag     = var.AUTOSHUTDOWN == true ? {} : { "cpt:tech:autoshutdown" = "false" }
  autoshutdown_asg_tag = var.AUTOSHUTDOWN == true ? {} : { key = "cpt:tech:autoshutdown", value = "false", propagate_at_launch = true }
  create_private_zone  = length(concat(var.EXTRA_INTERNAL_DNS_ALIASES,var.EXTRA_INTERNAL_TO_PUBLIC_DNS_ALIASES)) > 0
}

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
}

variable "AWS_ASSUME_ROLE" {
  description = "Role to which provider should be assumed"
  default     = ""
  type        = string
}

variable "PROJECT" {
  description = "Provide name of the project"
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "testing"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "SUFFIX" {
  type        = string
  description = "Optional Cluster Suffix, allows to create multiple clusters in the same project"
  default     = ""
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "SINGLE_NAT_GATEWAY" {
  default     = true
  description = "Define whether only single nat gateway should be created for prod envs should be false"
  type        = bool
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
  type        = string
}

variable "EXTRA_PUBLIC_DNS_ALIASES" {
  description = "List of extra aliases which should be added to public dns zone which will point to nginx ingress"
  type        = list(string)
  default     = []
}

variable "EXTRA_INTERNAL_DNS_ALIASES" {
  description = "List of extra aliases which should be added to private dns zone which will point to nginx ingress"
  type        = list(string)
  default     = []
}

variable "OFFICE_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of lodz office"
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
  type        = list(string)
}

variable "CI_CIDR_BLOCKS" {
  description = "Provide CIDR blocks of CI"
  default     = ["18.192.228.102/32", "18.158.189.113/32", "18.192.153.221/32", "18.193.144.231/32"]
  type        = list(string)
}

variable "EXTRA_CIDR_BLOCKS" {
  description = "Provide extra CIDR blocks"
  default     = []
  type        = list(string)
}

variable "KUBERNETES_VERSION" {
  description = "Version of kubernetes which should be deployed"
  type        = string
}

variable "CLUSTER_ADDONS" {
  description = "Map of cluster addon configurations to enable for the cluster. Addon name can be the map keys or set with `name`"
  type        = map(any)
  # versions adjusted for eks 1.21
  default = {
    vpc-cni = {
      addon_version     = "v1.10.1-eksbuild.1"
      resolve_conflicts = "OVERWRITE"
    }
    coredns = {
      addon_version     = "v1.8.4-eksbuild.1"
      resolve_conflicts = "OVERWRITE"
    }
    # TODO: adopt after fix (https://github.com/aws/containers-roadmap/issues/657)
    # kube-proxy = {
    #   addon_version     = "v1.21.2-eksbuild.2"
    #   resolve_conflicts = "OVERWRITE"
    # }
  }
}

variable "WARM_ENI_TARGET" {
  description = "Specifies the number of ENIs the daemon should keep ready"
  default     = 0
  type        = number
}

variable "WARM_IP_TARGET" {
  description = "Specifies the number of IPs that will be reserved for the node-ENI pair"
  default     = 1
  type        = number
}

variable "MINIMUM_IP_TARGET" {
  description = "Specifies how many minimum number of IPs to keep around each node"
  default     = 10
  type        = number
}

variable "APP_INGRESS_ACCESS_CIDR" {
  description = "Provide cidr block which should have access to ingress host"
  default     = ["0.0.0.0/0"]
  type        = list(string)
}

variable "KMS_SHARED_AMI_KEY_ID" {
  description = "KMS key used for shared images"
  type        = string
  default     = ""
}

variable "AMI_OWNER" {
  description = "Provide ami owner"
  type        = string
  default     = ""
}

variable "ADMIN_ROLES_ARN" {
  description = "Roles which should have access to kubernetes API"
  type        = list(string)
}

variable "PROMETHEUS_REMOTE_WRITE_ADDRESS" {
  type        = string
  default     = "http://thanos-receiver.thanos-receiver.svc.cluster.local:19291/api/v1/receive"
  description = "Address to which Prometheus will send data"
}

variable "PROMETHEUS_EXTERNAL_LABEL" {
  description = "Prometheus label used to thanos connection"
  type        = string

}

variable "PROMETHEUS_INGRESS_ENABLED" {
  description = "Whether prometheus UI should be exposed or not (only available via port forwarding)"
  type        = bool
  default     = false
}

variable "PROMETHEUS_INGRESS_WHITELIST" {
  description = "Provide whitelist for prometheus"
  type        = list(string)
  default     = ["194.145.235.0/24", "185.130.180.0/22"]
}

variable "ALERTMANAGER_CONFIG" {
  type        = string
  description = "Alertmanager custom config. Overrides defaults from prometheus-stack https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/values.yaml#L154"
  default     = ""
}

variable "MANAGEMENT_INSTANCE_TYPE" {
  description = "Provide kubernetes node instance type"
  default     = "t3.medium"
  type        = string
}

variable "MANAGEMENT_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "MANAGEMENT_ENABLE_SPOT_INSTANCES" {
  default     = false
  description = "This option enable spot instances"
  type        = bool
}

variable "MANAGEMENT_SPOT_MAX_PRICE" {
  default     = 0.048
  description = "Max spot price"
  type        = number
}

variable "MANAGEMENT_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 5
  type        = number
}

variable "MANAGEMENT_NODES_VOLUMES_SIZE" {
  description = "Provide size of the root partition in GB"
  default     = 20
  type        = number
}

variable "MANAGEMENT_NODES_VOLUMES_TYPE" {
  description = "Provide type of the root partition"
  default     = "gp2"
  type        = string
}

variable "APP_INSTANCE_TYPE" {
  description = "Provide kubernetes node instance type"
  default     = "t3.medium"
  type        = string
}

variable "APP_NR_OF_NODES" {
  description = "Provide number of kubernetes nodes"
  default     = "3"
  type        = string
}

variable "APP_MIN_NR_OF_NODES" {
  description = "Provide MIN number of kubernetes nodes"
  default     = 3
  type        = number
}

variable "APP_MAX_NR_OF_NODES" {
  description = "Provide MAX number of kubernetes nodes"
  default     = 10
  type        = number
}

variable "APP_NODES_VOLUMES_SIZE" {
  description = "Provide size of the root partition in GB"
  default     = 20
  type        = number
}

variable "APP_NODES_VOLUMES_TYPE" {
  description = "Provide type of the root partition"
  default     = "gp2"
  type        = string
}

variable "APP_ENABLE_SPOT_INSTANCES" {
  default     = false
  description = "This option enable spot instances"
  type        = bool
}

variable "APP_SPOT_MAX_PRICE" {
  default     = 0.048
  description = "Max spot price"
  type        = number
}

variable "MSK_PROTOCOL" {
  type        = string
  default     = "TLS"
  description = "Which protocol MSK should use"
}

variable "INSTALL_DEFAULT_APPS" {
  type        = bool
  default     = true
  description = "If true platform will install default applications like extrnal-dns/kong-ingress/istio/kubernetes-autoscaller..."
}

variable "LOGS_S3_BUCKET_DISABLE_HTTP_CONNECTION" {
  description = "Disable unecrypted (HTTP) communication"
  default     = true
  type        = bool
}

variable "LOG_GROUP_RETENTION" {
  description = "Log group retention in days"
  type        = number
  default     = 1
}

variable "LOG_GROUP_NAME" {
  description = "Log group name where string will be created"
  type        = string
  default     = "default-log-group"
}

variable "LOG_STREAM_NAME" {
  description = "Cloudwatch log stream name"
  type        = string
  default     = ""
}

variable "AWS_LOGGING_ENABLED" {
  description = "Enable logging for all AWS components"
  type        = bool
  default     = true
}


variable "DATABASE_CIDR_BLOCK" {
  description = "Database zone cidr"
  type        = list(string)
  default     = ["127.0.0.1/32"]
}

variable "AWS_CERT" {
  description = "Boolean value for enabling aws cert attached to alb and kong ingress"
  type        = bool
  default     = true
}

variable "ES_DOMAIN_ARN" {
  description = "ES domain arn"
  type        = string
  default     = ""
}

variable "ES_ENDPOINT" {
  description = "ES endpoint address"
  type        = string
  default     = ""
}

variable "NGINX_EXTERNAL_DNS_ADDRESSES" {
  description = "List of dns addresses which should be accessible from kong ingress"
  type        = list(string)
  default     = []
}

variable "LOCAL_NGINX_EXTERNAL_DNS_ADDRESSES" {
  description = "List of dns addresses which should be accessible from kong ingress"
  type        = list(string)
  default     = []
}

variable "DOCKER_REGISTRY_CREDENTIALS" {
  description = "Provide list of docker registries with credentials."
  default     = []
  type        = list(string)
}
variable "EXTRA_MSK_INGRESS_ACCESS_CIDRS" {
  description = "Provide extra cidr blocks which should have access to msk cluster"
  default     = []
  type        = list(string)
}

variable "KIALI_IMAGE_NAME" {
  description = "Name of Kiali image deployed by Kiali operator"
  type        = string
  default     = "docker-release.otlabs.fr/identity/kiali"
}

variable "KIALI_IMAGE_TAG" {
  description = "Tag of Kiali image deployed by Kiali operator"
  type        = string
  default     = "v1.30.0_1.0.1"
}

variable "COGNITO_API_KEY" {
  type        = string
  default     = ""
  description = "App api key for integration with corporate cognito"
}

variable "COGNITO_API_SECRET" {
  type        = string
  default     = ""
  description = "App api key secret for integration with corporate cognito"
}

variable "FEDERATED_COGNITO_DOMAIN" {
  type        = string
  default     = ""
  description = "Federated cognito public url"
}

variable "USER_POOL_ID" {
  type        = string
  default     = ""
  description = "User pool id"
}

variable "ISSUER_URL" {
  type        = string
  default     = ""
  description = "Cognito issuer URL"
}

variable "HERMES_ENABLED" {
  type        = bool
  default     = false
  description = "Hermes true/false value for enable deployment"
}

variable "DB_NAME" {
  type        = string
  description = "Database name"
  default     = "some_db"
}

variable "DB_ADMIN_USER" {
  type        = string
  description = "Database admin user"
  default     = "admin"
}

variable "EKS_APP_TARGET_GROUP_ARNS" {
  type        = list(string)
  description = "List of additional ingress ARN to add to target group"
  default     = []
}

variable "EKS_MANAGEMENT_TARGET_GROUP_ARNS" {
  type        = list(string)
  description = "List of additional ingress ARN to add to target group"
  default     = []
}

variable "DB_ADMIN_PASSWORD" {
  type        = string
  description = "Database admin password"
  default     = "password"
}

variable "DB_ALLOWED_CIDR_BLOCKS" {
  type        = list(string)
  description = "List of cidrs allowed to access database"
  default     = []
}

variable "DB_AUTOSCALING_ENABLED" {
  type        = bool
  default     = false
  description = "Whether to enable cluster autoscaling"
}

variable "DB_ENGINE_MODE" {
  type        = string
  default     = ""
  description = "The database engine mode. Valid values: `parallelquery`, `provisioned`, `serverless`, check regional availability for serverless"
}

variable "DB_ENGINE" {
  type        = string
  default     = ""
  description = "The name of the database engine to be used for this DB cluster. Valid values: `aurora`, `aurora-mysql`, `aurora-postgresql`, must be aurora for serverless mode"
}

variable "DB_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "DB_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "REDIS_GROUP_ID" {
  type        = string
  default     = "myredisgroupid"
  description = "Redis group id"
}

variable "ENABLE_COMMUNITY_WAF" {
  type        = bool
  default     = false
  description = "Boolean value, if set to true starts community WAF and attach it to ALB"
}

variable "ENABLE_DATABASES" {
  type        = bool
  default     = false
  description = "Boolean value, if set to true starts aurora"
}

variable "AUTH_TOKEN" {
  description = "Password for protected server"
  type        = string
  default     = null
}

variable "SCHEMA_REGISTRY_UI_ENABLED" {
  description = "Enable schema registry UI"
  type        = bool
  default     = false
}

variable "ENABLE_DELETION_PROTECTION" {
  description = "Enable this will prevent Terraform from deleting the load balancer"
  default     = true
  type        = bool
}

variable "MFA_ENABLED" {
  description = "Enable multi-factor authentication"
  default     = false
  type        = bool
}

variable "AUTOSHUTDOWN" {
  description = "Enable autoshutdown of instances"
  default     = false
  type        = bool
}

variable "INSTALL_DOCKER_CREDENTIALS" {
  type        = bool
  default     = true
  description = "If true platform will deploy secret called otlabs in default namespace with docker-registry credentials"
}


variable "INSTALL_CLUSTER_AUTOSCALER" {
  type        = bool
  default     = true
  description = "If true platform will install cluster autoscaler helm chart"
}

variable "INSTALL_EXTERNAL_DNS" {
  type        = bool
  default     = true
  description = "If true platform will install external dns helm chart"
}

variable "INSTALL_ISTIO" {
  type        = bool
  default     = true
  description = "If true platform will install istio helm chart"
}

variable "INSTALL_LOGGING" {
  type        = bool
  default     = true
  description = "If true platform will install logging operator helm chart"
}

variable "INSTALL_JAEGER" {
  type        = bool
  default     = true
  description = "If true platform will install jaeger helm chart"
}

variable "INSTALL_KIALI" {
  type        = bool
  default     = true
  description = "If true platform will install kiali-operator helm chart"
}

variable "INSTALL_NGINX_INGRESS" {
  type        = bool
  default     = true
  description = "If true platform will install ingress-nginx helm chart"
}

variable "INSTALL_KUBE2IAM" {
  type        = bool
  default     = true
  description = "If true platform will install kube2iam helm chart"
}

variable "INSTALL_KUBED" {
  type        = bool
  default     = true
  description = "If true platform will install kubed helm chart"
}

variable "INSTALL_VAULT" {
  type        = bool
  default     = true
  description = "If true platform will install vault helm chart"
}

variable "INSTALL_DEFAULT_RESOURCES_MUTATING_WEBHOOK" {
  type        = bool
  default     = true
  description = "If true platform will install default resources mutating webhook"
}

variable "ELASTICSEARCH_LOGGING_ENDPOINT" {
  description = "ES endpoint where logs will be pushed"
  default     = ""
  type        = string
}

variable "ELASTICSEARCH_LOGGING_REGION" {
  description = "ES region"
  default     = ""
  type        = string
}

#JAEGER variables
variable "ENABLED_JAEGER_INGRESS" {
  description = "Jaeger ingress used to access web ui"
  type        = bool
  default     = true
}

variable "JAEGER_CASSANDRA_URL" {
  description = "Jaeger cassandra url"
  type        = string
  default     = ""
}

variable "JAEGER_CASSANDRA_PORT" {
  description = "Jaeger cassandra port"
  type        = number
  default     = 9042
}

variable "JAEGER_CASSANDRA_USER" {
  description = "Jaeger cassandra user"
  type        = string
  default     = ""
}

variable "JAEGER_CASSANDRA_KEYSPACE" {
  description = "Jaeger cassandra keyspace"
  type        = string
  default     = "jaeger"
}

variable "JAEGER_ELASTICSEARCH_SCHEME" {
  description = "Jaeger Elasticsearch scheme"
  type        = string
  default     = "https"
}

variable "JAEGER_ELASTICSEARCH_URL" {
  description = "Jaeger Elasticsearch URL"
  type        = string
  default     = ""
}

variable "JAEGER_ELASTICSEARCH_PORT" {
  description = "Jaeger Elasticsearch port"
  type        = number
  default     = 443
}

variable "JAEGER_ELASTICSEARCH_USER" {
  description = "Jaeger Elasticsearch user"
  type        = string
  default     = ""
}

variable "JAEGER_ELASTICSEARCH_INDEX_PREFIX" {
  description = "Jaeger Elasticsearch index prefix"
  type        = string
  default     = ""
}

variable "OBSERVABILITY_LOGGING_ROLE" {
  description = "Logging observabitlity IAM role arn"
  default     = ""
  type        = string
}

variable "VAULT_ISTIO_INTEGRATION_ENABLED" {
  type        = bool
  default     = false
  description = "If true platform will use vault to sing istio mtls certificates"
}

variable "FLUENTD_REPLICAS" {
  description = "Number of replicas of fluentd"
  default     = 3
  type        = number
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "Provide additional ssh public keys"
  default     = []
  type        = list(string)
}

##NESSUS##
variable "NESSUS_KEY" {
  description = "Key used by nessus to authenticate"
  type        = string
  default     = "35e4c0a85554facf6e7c0a74cc43e54141dfe5da94d51273b26460df3a54547c"
}

variable "NESSUS_INSTANCE_TYPE" {
  description = "Type of ec2 instance used to setup nessus"
  type        = string
  default     = "t3.2xlarge"
}

variable "NESSUS_SG_EGRESS_RULES" {
  description = "List of addresses where nessus should have access"
  type        = list(string)
  default     = ["54.93.254.128/26:443", "18.194.95.64/26:443", "3.124.123.128/25:443", "3.67.7.128/25:443", "35.177.219.0/26:443", "3.9.159.128/25:443", "18.168.180.128/25:443", "18.168.224.128/25:443", "35.177.219.11/32:443", "35.177.219.10/32:443", "162.159.130.83/32:443", "162.159.129.83/32:443"]
}

variable "ENABLE_NESSUS" {
  description = "Boolean value, if set to true starts Nessus on environment"
  type        = bool
  default     = false
}

variable NESSUS_LAMBDAS_TRACING_MODE {
  default     = "PassThrough"
  description = "Choose tracing mode for nessus-start and nessus-stop lambdas. Valid Values: Active | PassThrough"
  type        = string
}

variable "ADDITIONAL_CLIENTS" {
  default     = []
  type        = list(any)
  description = "A list of additional app clients for a user pool. It supports the same values as a normal app resource"
}

variable "REDIS_ENGINE_VERSION" {
  description = "Used version of Redis Engine"
  type        = string
  default     = "5.0.6"
}

variable "REDIS_CUSTOM_PARAMETER_GROUP" {
  description = "Provide custom parameter group. May be empty, except using version redis 6.0x - should be default.redis6.x.cluster.on"
  default     = ""
  type        = string
}

variable REDIS_APPLY_IMMEDIATELY {
  default     = false
  type        = bool
  description = "Specifies whether any modifications are applied immediately, or during the next maintenance window. Default is false"
}

variable "REDIS_ENABLED" {
  type        = bool
  default     = false
  description = "Boolean value, if set to true starts redis"
}

variable "PRIVATE_ZONE_ADDITIONAL_VPCS" {
  default     = []
  description = "Additional VPCs authorized to associate with private DNS zone"
  type = list(object({
    id     = string,
    region = string
  }))
}

variable "ADDITIONAL_ASSOCIATED_ZONES" {
  default     = []
  description = "IDs of hosted zones which VPC will be associated with"
  type        = list(string)
}

variable "FLUENTD_CPU_LIMIT" {
  description = "CPU limit for Fluentd pod"
  type        = string
  default     = ""
}

variable "FLUENTD_CPU_REQ" {
  description = "CPU request for Fluentd pod"
  type        = number
  default     = 1
}

variable "FLUENTD_CONFIG_RELOADER_CPU_REQUEST" {
  description = "CPU request for Fluentd config reloader container"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "FLUENTD_CONFIG_RELOADER_CPU_LIMIT" {
  description = "CPU limit for Fluentd config reloader container"
  type        = string
  default     = ""
}


variable "FLOW_LOGS_ENABLED" {
  default     = false
  description = "Define whether vpc flow logs should be enabled"
  type        = bool
}

variable "FLOW_LOGS_BUCKET_ARN" {
  default     = ""
  description = "Provide vpc flow log s3 bucket arn"
  type        = string
}

variable "FLOW_LOGS_TRAFFIC_TYPE" {
  default     = "REJECT"
  description = "Provide type of vpc traffic to capture (ACCEPT, REJECT, ALL)"
  type        = string
}

variable "FLUENTD_RETRY_FOREVER" {
  description = "Fluentd buffer retry forever"
  type        = bool
  default     = false
}

variable "FLUENTD_TIMEKEY_WAIT" {
  description = "Fluentd timekey wait"
  type        = string
  default     = "15s"
}

variable "FLUENTD_RETRY_WAIT" {
  description = "Fluentd request retry wait"
  type        = string
  default     = "20s"
}

variable "FLUENTD_RETRY_TIMEOUT" {
  description = "Fluentd request retry timeout"
  type        = string
  default     = "24h"
}

variable "EXTRA_INTERNAL_TO_PUBLIC_DNS_ALIASES" {
  description = "List of extra aliases which should be added to private dns zone which will point to nginx ingress"
  type        = list(string)
  default     = []
}

variable "DEFAULT_CPU_REQUEST" {
  description = "Default CPU request applied by mutating webhook"
  type        = string
  default     = "250m" #string_type_lint_skip
}

variable "DEFAULT_CPU_LIMIT" {
  description = "Default CPU limit applied by mutating webhook"
  type        = string
  default     = ""
}


variable "CLUSTER_AUTOSCALER_CPU_REQUEST" {
  description = "CPU request for cluster autoscaler"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "CLUSTER_AUTOSCALER_CPU_LIMIT" {
  description = "CPU limit for cluster autoscaler"
  type        = string
  default     = ""
}

variable "EXTERNAL_DNS_CPU_REQUEST" {
  description = "CPU request for external DNS"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "EXTERNAL_DNS_CPU_LIMIT" {
  description = "CPU limit for external DNS"
  type        = string
  default     = ""
}


variable "KUBE2IAM_CPU_REQUEST" {
  description = "CPU request for kube2iam"
  type        = string
  default     = "20m" #string_type_lint_skip
}

variable "KUBE2IAM_CPU_LIMIT" {
  description = "CPU limit for kube2iam"
  type        = string
  default     = ""
}


variable "ISTIO_OPERATOR_CPU_REQUEST" {
  description = "CPU request for istio operator"
  type        = string
  default     = "50m" #string_type_lint_skip
}

variable "ISTIO_OPERATOR_CPU_LIMIT" {
  description = "CPU limit for istio operator"
  type        = string
  default     = ""
}


variable "LOGGING_OPERATOR_CPU_REQUEST" {
  description = "CPU request for logging operator"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "LOGGING_OPERATOR_CPU_LIMIT" {
  description = "CPU limit for logging operator"
  type        = string
  default     = ""
}


variable "JAEGER_SCHEMA_CPU_REQUEST" {
  description = "CPU request for jaeger schema"
  type        = string
  default     = "256m" #string_type_lint_skip
}

variable "JAEGER_SCHEMA_CPU_LIMIT" {
  description = "CPU limit for jaeger schema"
  type        = string
  default     = ""
}

variable "JAEGER_AGENT_CPU_REQUEST" {
  description = "CPU request for jaeger agent"
  type        = string
  default     = "50m" #string_type_lint_skip
}

variable "JAEGER_AGENT_CPU_LIMIT" {
  description = "CPU limit for jaeger agent"
  type        = string
  default     = ""
}


variable "JAEGER_COLLECTOR_CPU_REQUEST" {
  description = "CPU request for jaeger collector"
  type        = string
  default     = "500m" #string_type_lint_skip
}

variable "JAEGER_COLLECTOR_CPU_LIMIT" {
  description = "CPU limit for jaeger collector"
  type        = string
  default     = ""
}


variable "JAEGER_QUERY_CPU_REQUEST" {
  description = "CPU request for jaeger query"
  type        = string
  default     = "256m" #string_type_lint_skip
}

variable "JAEGER_QUERY_CPU_LIMIT" {
  description = "CPU limit for jaeger query"
  type        = string
  default     = ""
}

variable "JAEGER_CASSANDRA_CPU_REQUEST" {
  description = "CPU request for jaeger cassandra"
  type        = string
  default     = "1" #string_type_lint_skip
}

variable "JAEGER_CASSANDRA_CPU_LIMIT" {
  description = "CPU limit for jaeger cassandra"
  type        = string
  default     = ""
}


variable "KIALI_OPERATOR_CPU_REQUEST" {
  description = "CPU request for kiali operator"
  type        = string
  default     = "250m" #string_type_lint_skip
}

variable "KIALI_OPERATOR_CPU_LIMIT" {
  description = "CPU limit for kiali operator"
  type        = string
  default     = ""
}


variable "KIALI_CPU_REQUEST" {
  description = "CPU request for kiali operator"
  type        = string
  default     = "10m" #string_type_lint_skip
}

variable "KIALI_CPU_LIMIT" {
  description = "CPU limit for kiali operator"
  type        = string
  default     = ""
}


variable "NGINX_INGRESS_CPU_REQUEST" {
  description = "CPU request for nginx ingress"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "NGINX_INGRESS_CPU_LIMIT" {
  description = "CPU limit for nginx ingress"
  type        = string
  default     = ""
}


variable "KUBED_CPU_REQUEST" {
  description = "CPU request for kubed"
  type        = string
  default     = "250m" #string_type_lint_skip
}

variable "KUBED_CPU_LIMIT" {
  description = "CPU limit for kubed"
  type        = string
  default     = ""
}


variable "VAULT_INJECTOR_CPU_REQUEST" {
  description = "CPU request for vault injector"
  type        = string
  default     = "250m" #string_type_lint_skip
}

variable "VAULT_INJECTOR_CPU_LIMIT" {
  description = "CPU limit for vault injector"
  type        = string
  default     = ""
}


variable "VAULT_SERVER_CPU_REQUEST" {
  description = "CPU request for vault server"
  type        = string
  default     = "250m" #string_type_lint_skip
}

variable "VAULT_SERVER_CPU_LIMIT" {
  description = "CPU limit for vault server"
  type        = string
  default     = ""
}


variable "ISTIO_PROMETHEUS_SERVER_CPU_REQUEST" {
  description = "CPU request for Prometheus deployed along with istio profile"
  type        = string
  default     = "500m" #string_type_lint_skip
}

variable "ISTIO_PROMETHEUS_SERVER_CPU_LIMIT" {
  description = "CPU limit for Prometheus deployed along with istio profile"
  type        = string
  default     = ""
}

variable "ISTIO_PROMETHEUS_CONFIG_RELOADER_CPU_REQUEST" {
  description = "CPU request for Prometheus config reloader deployed along with istio profile"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "ISTIO_PROMETHEUS_CONFIG_RELOADER_CPU_LIMIT" {
  description = "CPU limit for Prometheus config reloader deployed along with istio profile"
  type        = string
  default     = ""
}


variable "DEFAULT_RESOURCES_MUTATING_WEBHOOK_CPU_REQUEST" {
  description = "CPU request for default resources mutating webhook"
  type        = string
  default     = "100m"
}

variable "DEFAULT_RESOURCES_MUTATING_WEBHOOK_CPU_LIMIT" {
  description = "CPU limit for default resources mutating webhook"
  type        = string
  default     = ""
}


variable "ARGOCD_CONTROLLER_CPU_REQUEST" {
  description = "CPU request for Argo CD application controller"
  type        = string
  default     = "500m" #string_type_lint_skip
}

variable "ARGOCD_CONTROLLER_CPU_LIMIT" {
  description = "CPU limit for Argo CD application controller"
  type        = string
  default     = ""
}

variable "ARGOCD_CONTROLLER_MEMORY_REQUEST" {
  description = "Memory request for Argo CD application controller"
  type        = string
  default     = "1Gi"
}

variable "ARGOCD_CONTROLLER_MEMORY_LIMIT" {
  description = "Memory limit for Argo CD application controller"
  type        = string
  default     = "2Gi"
}

variable "ARGOCD_DEX_CPU_REQUEST" {
  description = "CPU request for Argo CD dex"
  type        = string
  default     = "50m" #string_type_lint_skip
}

variable "ARGOCD_DEX_CPU_LIMIT" {
  description = "CPU limit for Argo CD dex"
  type        = string
  default     = ""
}

variable "ARGOCD_DEX_MEMORY_REQUEST" {
  description = "Memory request for Argo CD dex"
  type        = string
  default     = "256Mi"
}

variable "ARGOCD_DEX_MEMORY_LIMIT" {
  description = "Memory limit for Argo CD dex"
  type        = string
  default     = "512Mi"
}

variable "ARGOCD_REDIS_CPU_REQUEST" {
  description = "CPU request for Argo CD redis"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "ARGOCD_REDIS_CPU_LIMIT" {
  description = "CPU limit for Argo CD redis"
  type        = string
  default     = ""
}

variable "ARGOCD_REDIS_MEMORY_REQUEST" {
  description = "Memory request for Argo CD redis"
  type        = string
  default     = "64Mi"
}

variable "ARGOCD_REDIS_MEMORY_LIMIT" {
  description = "Memory limit for Argo CD redis"
  type        = string
  default     = "128Mi"
}

variable "ARGOCD_SERVER_CPU_REQUEST" {
  description = "CPU request for Argo CD server"
  type        = string
  default     = "100m" #string_type_lint_skip
}

variable "ARGOCD_SERVER_CPU_LIMIT" {
  description = "CPU limit for Argo CD server"
  type        = string
  default     = ""
}

variable "ARGOCD_SERVER_MEMORY_REQUEST" {
  description = "Memory request for Argo CD server"
  type        = string
  default     = "128Mi"
}

variable "ARGOCD_SERVER_MEMORY_LIMIT" {
  description = "Memory limit for Argo CD server"
  type        = string
  default     = "256Mi"
}

variable "ARGOCD_REPO_SERVER_CPU_REQUEST" {
  description = "CPU request for Argo CD repo server"
  type        = string
  default     = "400m" #string_type_lint_skip
}

variable "ARGOCD_REPO_SERVER_CPU_LIMIT" {
  description = "CPU limit for Argo CD repo server"
  type        = string
  default     = ""
}

variable "ARGOCD_REPO_SERVER_MEMORY_REQUEST" {
  description = "Memory request for Argo CD repo server"
  type        = string
  default     = "512Mi"
}

variable "ARGOCD_REPO_SERVER_MEMORY_LIMIT" {
  description = "Memory limit for Argo CD repo server"
  type        = string
  default     = "1Gi"
}

variable "ALERTMANAGER_CPU_REQUEST" {
  type        = string
  default     = "10m" #string_type_lint_skip
  description = "CPU request for alert manager"
}

variable "ALERTMANAGER_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for alermanager"
}

variable "ALERTMANAGER_MEMORY_REQUEST" {
  type        = string
  default     = "64Mi"
  description = "Memory request for alert manager"
}

variable "ALERTMANAGER_MEMORY_LIMIT" {
  type        = string
  default     = "256Mi"
  description = "Memory limit for alert manager"
}

variable "PROMETHEUS_CPU_REQUEST" {
  type        = string
  default     = "500m" #string_type_lint_skip
  description = "CPU request for prometheus server"
}

variable "PROMETHEUS_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for prometheus server"
}

variable "PROMETHEUS_MEMORY_REQUEST" {
  type        = string
  default     = "1Gi"
  description = "Memory request for prometheus server"
}

variable "PROMETHEUS_MEMORY_LIMIT" {
  type        = string
  default     = "2Gi"
  description = "Memory limit for prometheus server"
}

variable "NODE_EXPORTER_CPU_REQUEST" {
  type        = string
  default     = "100m" #string_type_lint_skip
  description = "CPU request for node exporter"
}

variable "NODE_EXPORTER_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for node exporter"
}

variable "NODE_EXPORTER_MEMORY_REQUEST" {
  type        = string
  default     = "30Mi"
  description = "Memory request for node exporter"
}

variable "NODE_EXPORTER_MEMORY_LIMIT" {
  type        = string
  default     = "50Mi"
  description = "Memory limit for node exporter"
}

variable "KUBE_STATE_METRICS_CPU_REQUEST" {
  type        = string
  default     = "10m" #string_type_lint_skip
  description = "CPU request for kube state metrics"
}

variable "KUBE_STATE_METRICS_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for kube state metrics"
}

variable "KUBE_STATE_METRICS_MEMORY_REQUEST" {
  type        = string
  default     = "128Mi"
  description = "Memory request for kube state metrics"
}

variable "KUBE_STATE_METRICS_MEMORY_LIMIT" {
  type        = string
  default     = "256Mi"
  description = "Memory limit for kube state metrics"
}

variable "PROMETHEUS_OPERATOR_CPU_REQUEST" {
  type        = string
  default     = "100m" #string_type_lint_skip
  description = "CPU request for prometheus operator"
}

variable "PROMETHEUS_OPERATOR_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for prometheus operator"
}

variable "PROMETHEUS_OPERATOR_MEMORY_REQUEST" {
  type        = string
  default     = "256Mi"
  description = "Memory request for prometheus operator"
}

variable "PROMETHEUS_OPERATOR_MEMORY_LIMIT" {
  type        = string
  default     = "512Mi"
  description = "Memory limit for prometheus operator"
}

variable "GRAFANA_CPU_REQUEST" {
  type        = string
  default     = "100m" #string_type_lint_skip
  description = "CPU request for grafana"
}

variable "GRAFANA_CPU_LIMIT" {
  type        = string
  default     = ""
  description = "CPU limit for grafana"
}

variable "GRAFANA_MEMORY_REQUEST" {
  type        = string
  default     = "128Mi"
  description = "Memory request for grafana"
}

variable "GRAFANA_MEMORY_LIMIT" {
  type        = string
  default     = "256Mi"
  description = "Memory limit for grafana"
}

variable "IGNORE_MUTATING_WEBHOOKS" {
  description = "A boolean value that makes ArgoCD diff tool ignore mutating webhooks."
  type        = bool
  default     = true
}

variable "ISTIO_INGRESS_ALB_BACKEND_PORT" {
  type        = number
  default     = 32080
  description = "Node port where Istio ingress gateway will listen to requests"
}

variable "ISTIO_INGRESS_ALB_BACKEND_PROTOCOL" {
  type        = string
  default     = "HTTP"
  description = "Provide Istio Ingress load balancer backend protocol, ex TLS, HTTP"
}

variable "ISTIO_INGRESS_ALB_HEALTH_CHECK_PORT" {
  type        = number
  default     = 32320
  description = "Istio Ingress load balancer health check port"
}

variable "FLUENTD_MEMORY" {
  description = "Memory request and limit limit for Fluentd pod"
  type        = string
  default     = "1Gi"
}

variable "FLUENTD_CONFIG_RELOADER_MEMORY" {
  description = "Memory request and limit for Fluentd config reloader container"
  type        = string
  default     = "32Mi"
}

variable "DEFAULT_MEMORY" {
  description = "Default memory request and limit applied by mutating webhook"
  type        = string
  default     = "256Mi"
}

variable "CLUSTER_AUTOSCALER_MEMORY" {
  description = "Memory request and limit for cluster autoscaler"
  type        = string
  default     = "512Mi"
}

variable "ISTIO_OPERATOR_MEMORY" {
  description = "Memory request and limit for istio operator"
  type        = string
  default     = "128Mi"
}

variable "LOGGING_OPERATOR_MEMORY" {
  description = "Memory request and limit for logging operator"
  type        = string
  default     = "256Mi"
}

variable "JAEGER_SCHEMA_MEMORY" {
  description = "Memory request and limit for jaeger schema"
  type        = string
  default     = "128Mi"
}

variable "JAEGER_AGENT_MEMORY" {
  description = "Memory request for jaeger agent"
  type        = string
  default     = "64Mi"
}

variable "JAEGER_COLLECTOR_MEMORY" {
  description = "Memory request and limit for jaeger collector"
  type        = string
  default     = "512Mi"
}

variable "JAEGER_QUERY_MEMORY" {
  description = "Memory request for jaeger query"
  type        = string
  default     = "128Mi"
}

variable "JAEGER_CASSANDRA_MEMORY" {
  description = "Memory request and limit for jaeger cassandra"
  type        = string
  default     = "4Gi"
}

variable "KIALI_OPERATOR_MEMORY" {
  description = "Memory request and limit for kiali operator"
  type        = string
  default     = "512Mi"
}

variable "KIALI_MEMORY" {
  description = "Memory request and limit for kiali operator"
  type        = string
  default     = "64Mi"
}

variable "NGINX_INGRESS_MEMORY" {
  description = "Memory request and limit for nginx ingress"
  type        = string
  default     = "100Mi"
}

variable "KUBED_MEMORY" {
  description = "Memory request and limit for kubed"
  type        = string
  default     = "512Mi"
}

variable "VAULT_INJECTOR_MEMORY" {
  description = "Memory request and for vault injector"
  type        = string
  default     = "256Mi"
}

variable "VAULT_SERVER_MEMORY" {
  description = "Memory request and limit for vault server"
  type        = string
  default     = "256Mi"
}

variable "ISTIO_PROMETHEUS_SERVER_MEMORY" {
  description = "Memory request and limit for Prometheus deployed along with istio profile"
  type        = string
  default     = "1Gi"
}

variable "ISTIO_PROMETHEUS_CONFIG_RELOADER_MEMORY" {
  description = "Memory request for Prometheus config reloader deployed along with istio profile"
  type        = string
  default     = "256Mi"
}

variable "DEFAULT_RESOURCES_MUTATING_WEBHOOK_MEMORY" {
  description = "Memory request and limit for default resources mutating webhook"
  type        = string
  default     = "256Mi"
}

variable "EXTERNAL_DNS_MEMORY" {
  description = "Memory request and limit for external DNS"
  type        = string
  default     = "128Mi"
}

variable "KUBE2IAM_MEMORY" {
  description = "Memory request and limit for kube2iam"
  type        = string
  default     = "32Mi"
}
